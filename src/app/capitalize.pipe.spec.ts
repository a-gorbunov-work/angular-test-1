import { CapitalizePipe } from './capitalize.pipe';

describe('CapitalizePipe', () => {
  it('create an instance', () => {
    const pipe = new CapitalizePipe();
    expect(pipe).toBeTruthy();
  });

  it('capitalizes single word', () => {
    const pipe = new CapitalizePipe();
    expect(pipe.transform('small')).toEqual('Small');
  });
});
