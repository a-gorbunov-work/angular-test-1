import { HttpClientModule } from '@angular/common/http';
import { TestBed, inject } from '@angular/core/testing';

import { DogsBackendService } from './dogs-backend.service';

describe('DogsBackendService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        DogsBackendService
      ],
      imports: [
        HttpClientModule
      ]
    });
  });

  it('should be created', inject([DogsBackendService], (service: DogsBackendService) => {
    expect(service).toBeTruthy();
  }));
});
