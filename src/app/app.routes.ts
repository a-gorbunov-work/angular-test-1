import { BreedDemoComponent } from './breed-demo/breed-demo.component';
import { Routes } from '@angular/router';

export const appRoutes: Routes = [
    { path: '', component: BreedDemoComponent },
    { path: ':breed', component: BreedDemoComponent }
];
