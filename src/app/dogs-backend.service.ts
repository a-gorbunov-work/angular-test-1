import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class DogsBackendService {
  private baseUri = 'https://dog.ceo/api';
  constructor(private http: HttpClient) { }

  public getBreedList(): Observable<IBreed[]> {
    return this.http
      .get<IBreedListResponse>(`${this.baseUri}/breeds/list/all`)
      .map(this.transformBreedListResponse.bind(this));
  }

  public getRandomBreedImageURL(breed: IBreed): Observable<string> {
    return this.http
      .get<IBreedListResponse>(`${this.baseUri}/breed/${breed.path}/images/random`)
      .map(this.transformBreedImageResponse.bind(this));
  }

  protected transformBreedListResponse(response: IBreedListResponse) {
    if (response.status !== 'success') {
      throw new Error('Error while getting breeds list');
    }

    return Object.keys(response.message)
      /* Each breed maps into IDogsListItem[] */
      .map(breed => this.createBreedListItems(breed, response.message[breed]))
      /* Then flat an array: IDogsListItem[][] => IDogsListItem[] */
      .reduce((result, breeds) => result.concat(...breeds), []);
  }

  protected createBreedListItems(breed: string, subbreeds: string[]) {
    if (subbreeds.length === 0) {
      /* If breed has no subs, add single item to result ... */
      return [{
        title: breed,
        path: breed,
        routeParam: breed
      }];
    } else {
      /* ... else add each subbreed to result */
      return subbreeds.map(subbreed => {
        return {
          title: `${subbreed} ${breed}`,
          path: `${breed}/${subbreed}`,
          routeParam: `${subbreed}-${breed}`
        };
      });
    }
  }

  protected transformBreedImageResponse(response: IBreedImageResponse) {
    if (response.status !== 'success') {
      throw new Error('Error while getting breeds list');
    }
    return response.message;
  }
}

export interface IBreed {
  title: string;
  path: string;
  routeParam: string;
}

interface IBreedListResponse {
  status: string;
  message: {[breed: string]: string[]};
}

interface IBreedImageResponse {
  status: string;
  message: string;
}
