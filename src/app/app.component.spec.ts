import { CapitalizePipe } from './capitalize.pipe';
import { FormsModule } from '@angular/forms';
import { BreedDemoComponent } from './breed-demo/breed-demo.component';
import { RouterModule } from '@angular/router';
import { TestBed, async } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';

import { AppComponent } from './app.component';
import { appRoutes } from './app.routes';

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent,
        BreedDemoComponent,
        CapitalizePipe
      ],
      imports: [
        FormsModule,
        RouterTestingModule.withRoutes(appRoutes)
      ],
    }).compileComponents();
  }));

  it('should create the app', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));
});
