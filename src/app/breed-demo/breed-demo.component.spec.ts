import { HttpClientModule } from '@angular/common/http';
import { DogsBackendService } from './../dogs-backend.service';
import { appRoutes } from './../app.routes';
import { CapitalizePipe } from './../capitalize.pipe';
import { FormsModule } from '@angular/forms';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';

import { BreedDemoComponent } from './breed-demo.component';

describe('BreedDemoComponent', () => {
  let component: BreedDemoComponent;
  let fixture: ComponentFixture<BreedDemoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        BreedDemoComponent,
        CapitalizePipe,
      ],
      imports: [
        FormsModule,
        RouterTestingModule.withRoutes(appRoutes),
        HttpClientModule
      ],
      providers: [
        DogsBackendService
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BreedDemoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
