import { DogsBackendService, IBreed } from './../dogs-backend.service';
import { ActivatedRoute, Router, ParamMap } from '@angular/router';
import { ISubscription } from 'rxjs/Subscription';
import { Component, OnInit, OnDestroy } from '@angular/core';

@Component({
  selector: 'app-breed-demo',
  templateUrl: './breed-demo.component.html',
  styleUrls: ['./breed-demo.component.scss']
})
export class BreedDemoComponent implements OnInit, OnDestroy {

  protected queryChangeSub: ISubscription;
  protected currentBreedName = '';
  protected breedList: IBreed[];
  protected breedImage: string;
  protected errorOccured = false;

  public constructor(
    private route: ActivatedRoute,
    private router: Router,
    private dogsBackend: DogsBackendService) {}

  public ngOnInit(): void {
    this.dogsBackend.getBreedList()
      .toPromise()
      .then(this.handleListLoad.bind(this))
      .catch(err => {
        /* Handle an error: send report, etc.. */
        this.errorOccured = true;
        throw err;
      });
  }

  public ngOnDestroy(): void {
    this.queryChangeSub.unsubscribe();
  }

  public changeBreed(breedRoutePath: string) {
    this.router.navigate(['/', breedRoutePath]);
  }

  protected async handleListLoad(breedList: IBreed[]) {
    this.breedList = breedList;
    this.createRandomImage();
    this.queryChangeSub = this.route.paramMap.subscribe(this.handleRouteParamsChange.bind(this));
  }

  protected async handleRouteParamsChange(params: ParamMap) {
    if (!params.has('breed')) {
      return;
    }

    if (!this.breedList.some(item => item.routeParam === params.get('breed'))) {
      /* Breed from URL param not found, navigate to default page */
      this.changeBreed('');
      return;
    }

    this.currentBreedName = params.get('breed');
    this.createRandomImage();
  }

  protected async createRandomImage() {
    if (!Array.isArray(this.breedList)) {
      return;
    }

    const currentBreed = this.breedList.find(item => item.routeParam === this.currentBreedName);
    if (typeof currentBreed === 'undefined') {
      /* Not found */
      return;
    }

    this.breedImage = await this.dogsBackend.getRandomBreedImageURL(currentBreed).toPromise();
  }
}
